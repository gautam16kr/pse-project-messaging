﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Masti.QualityAssurance;


namespace Masti.ImageProcessing
{
    /// <summary>
    /// This class provides test cases for compression class.
    /// </summary>
    public class ReceiveImageTest : ITest
    {
        /// <summary>
        /// The logger is used for Logging functionality within the test.
        /// Helps in debugging of tests.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReceiveImageTest" /> class.
        /// </summary>
        /// <param name="logger">Assigns the Logger to be used by the Test</param>
        public ReceiveImageTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>Returns whether the status is successful or not.</returns>
        public bool Run()
        {

            return false;
        }
    }
}
