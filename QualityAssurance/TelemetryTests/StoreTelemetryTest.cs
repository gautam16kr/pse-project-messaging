﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     16th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="StoreTelemetryTest.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class tests whether telemetry storing works properly.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance.TelemetryTests
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Class to test storing functionality of telemetry.
    /// </summary>
    public class StoreTelemetryTest : ITest
    {
        /// <summary>
        /// Logger instance used to log messages while testing.
        /// </summary>
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the <see cref="StoreTelemetryTest" /> class.
        /// </summary>
        /// <param name="logger">The logger instance to be used to log messages while testing.</param>
        public StoreTelemetryTest(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Runs tests to check update functionality of telemetry.
        /// </summary>
        /// <returns>Success status of tests run.</returns>
        public bool Run()
        {
            // Store telemetry to file. 
            this.logger.LogInfo("Storing telemetry to file...");

            // Stores success status of this test.
            var storedSuccessfully = false;
            TelemetryCollector telemetryCollector = TelemetryCollector.Instance;
            
            if (telemetryCollector.GetTelemetryObject("SampleTelemetry") == null)
            {
                telemetryCollector.RegisterTelemetry("SampleTelemetry", new SampleTelemetry());
            }

            var sampleTelemetry = (SampleTelemetry)telemetryCollector.GetTelemetryObject("SampleTelemetry");

            // Update the quantity.
            sampleTelemetry.IncrementQuantity();
            sampleTelemetry.IncrementQuantity();


            try
            {
                Dictionary<string, ITelemetry> beforeStoring = (Dictionary<string, ITelemetry>)telemetryCollector.RegisteredTelemetry;

                // Find out name of next telemetry file to be created.

                // Path to base directory.
                string directory = System.AppContext.BaseDirectory;

                // Get current date.
                string date = DateTime.Now.ToString("MM_dd_yyyy", CultureInfo.CurrentCulture);

                // Select files in the base directory belonging to this date.
                string[] files = Directory.GetFiles(directory, "Telemetry_" + date + '*');

                // Number of next telemetry file.
                var telemetryNumber = 0;

                // Check if any telemetry files are already present.
                if (files != null && files.Length != 0)
                {
                    // Get next telemetry number.
                    string[] tokens = files.Max().Split('_');
                    string token = tokens[tokens.Length - 1];
                    telemetryNumber = int.Parse(token.Split('.')[0], CultureInfo.CurrentCulture) + 1;
                }

                // Generate file name.
                var fileName = string.Format(CultureInfo.CurrentCulture, "Telemetry_{0}_{1}.json", date, telemetryNumber);
                var filePath = directory + fileName;

                // Store telemetry.
                bool stored = telemetryCollector.StoreTelemetry();

                // Check if stored properly.
                if (stored == false)
                {
                    this.logger.LogWarning("Storing failed. Functionality not working as expected!");

                    // Test failed.
                    return false;
                }

                // Holds contents of the telemetry file.
                string contents = string.Empty;

                try
                {
                    // Get all contents from file. 
                    contents = System.IO.File.ReadAllText(filePath);
                }
                catch(FileNotFoundException)
                {
                    MastiDiagnostics.LogWarning("Telemetry file was not created.");
                    this.logger.LogWarning("File was not created. Functionality did not work as intended!");

                    // Test failed.
                    return false;
                }

                // Deserialize contents.
                //IDictionary<string, ITelemetry> retrievedTelemetries = JsonConvert.DeserializeObject<Dictionary<string, ITelemetry>>(contents);
                var retrievedTelemetries = JsonConvert.DeserializeObject<Dictionary<string, SampleTelemetry>>(contents);

                // Check if they are equal.
                storedSuccessfully = this.Check(retrievedTelemetries, beforeStoring);

            }
            catch (Exception e)
            {
                MastiDiagnostics.LogWarning(string.Format(CultureInfo.InvariantCulture, "Couldn't compare with file contents. {0}", e.Message));
                this.logger.LogWarning("Couldn't check whether telemetry stored correctly.");
            }
            
            if (storedSuccessfully)
            {
                this.logger.LogSuccess("Stored successfully. Functionality works as intended.");
            }
            return storedSuccessfully;
        }

        /// <summary>
        /// Check if two dictionaries are equal.
        /// </summary>
        /// <typeparam name="TKey">Type of key.</typeparam>
        /// <typeparam name="TValue">Type of value.</typeparam>
        /// <param name="x">First dictionary.</param>
        /// <param name="y">Second dictionary.</param>
        /// <returns></returns>
        private bool Check(IDictionary<string, SampleTelemetry> x, IDictionary<string, ITelemetry> y)
        {
            // Early-exit checks.
            if (null == y)
                return null == x;
            if (null == x)
                return false;
            if (object.ReferenceEquals(x, y))
                return true;
            if (x.Count != y.Count)
                return false;

            // Check keys are the same.
            foreach (string k in x.Keys)
                if (!y.ContainsKey(k))
                    return false;

            // Check values are the same.
            foreach (string k in x.Keys)
            {
                var yk = (SampleTelemetry)y[k];
                var xk = (SampleTelemetry)x[k];
                if (!xk.Equals(yk))
                    return false;
            }
                

            return true;
        }
    }
}