﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//     16th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="SampleTelemetry.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This is a sample implementation of ITelemetry interface used for 
//    testing telemetry functionalities.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance.TelemetryTests
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Sample class telemetry class used to test telemetry functionalities.
    /// </summary>
    public class SampleTelemetry : ITelemetry
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SampleTelemetry" /> class.
        /// </summary>
        public SampleTelemetry()
        {
                this.DataCapture = new Dictionary<string, string>();
                this.DataCapture.Add("Quantity", "0");
        }

        /// <summary>
        /// Gets or sets telemetry data pertaining to this class.
        /// </summary>
        public IDictionary<string, string> DataCapture { get; set; }

        /// <summary>
        /// Increment telemetry value of Quantity by 1.
        /// </summary>
        public void IncrementQuantity()
        {
            var currentVal = int.Parse(this.DataCapture["Quantity"], CultureInfo.InvariantCulture);
            this.DataCapture["Quantity"] = (currentVal + 1).ToString(CultureInfo.InvariantCulture);
            MastiDiagnostics.LogInfo(string.Format(CultureInfo.CurrentCulture, "Quantity incremented to {0}.", currentVal + 1));
        }

        /// <summary>
        /// Checks whether given object has same values as self.
        /// </summary>
        /// <param name="y">Object to be compared with.</param>
        /// <returns>Success status.</returns>
        public bool Equals(SampleTelemetry y)
        {
            return this.DataCapture["Quantity"] == y.DataCapture["Quantity"];
        }
    }
}
