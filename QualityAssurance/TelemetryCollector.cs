﻿//-----------------------------------------------------------------------
// <author> 
//    Anish M M (anishmathewdev@gmail.com)
// </author>
//
// <date> 
//    11th November, 2018
// </date>
// 
// <reviewer> 
//
// </reviewer>
// 
// <copyright file="TelemetryCollector.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//    This class collects and stores telemetry information from all other modules.
//    It also provides functionality to write the accumulated telemety into a 
//    file in JSON serialized format.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.QualityAssurance
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Newtonsoft.Json;

    /// <summary>
    /// Register and store Telemetry data of each module 
    /// and write to a file at the end of the program's execution. 
    /// </summary>
    public sealed class TelemetryCollector : ITelemetryCollector
    {
        /// <summary>
        /// Single instance of TelemetryCollector to be used globally.
        /// </summary>
        private static TelemetryCollector instance = new TelemetryCollector();

        /// <summary>
        /// Gets and sets the ITelemetry instances of each of the modules.
        /// </summary>
        public IDictionary<string, ITelemetry> RegisteredTelemetry { get; set; } = new Dictionary<string, ITelemetry>();

        /// <summary>
        /// Initializes static members of the <see cref="TelemetryCollector"/> class. 
        /// </summary>
        static TelemetryCollector()
        {
        }

        /// <summary>
        /// Prevents a default instance of the <see cref="TelemetryCollector"/> class from being created.
        /// </summary>
        private TelemetryCollector()
        {
        }

        /// <summary>
        /// Gets or sets instance.
        /// </summary>
        public static TelemetryCollector Instance
        {
            get
            {
                return instance;
            }

            set
            {
                instance = value;
            }
        }
        
        /// <summary>
        /// Return the telemetry object for a module.
        /// </summary>
        /// <param name="telemetryObjectName">
        /// String key of the Telemetry to be returned.
        /// </param>
        /// <returns>Return the ITelemetry object given the string key.</returns>
        public ITelemetry GetTelemetryObject(string telemetryObjectName)
        {
            if (Instance.RegisteredTelemetry.TryGetValue(telemetryObjectName, out ITelemetry telemetryValue))
            {
                MastiDiagnostics.LogSuccess(string.Format(
                    CultureInfo.CurrentCulture,
                    "Telemetry for requested module ({0}) found.", 
                    telemetryObjectName));
            }
            else
            {
                MastiDiagnostics.LogWarning(string.Format(
                    CultureInfo.CurrentCulture,
                    "Telemetry for requested module ({0}) not found.",
                    telemetryObjectName));
            }

            return telemetryValue;
        }

        /// <summary>
        /// Register the Telemetry object for a module referenced 
        /// with the given string key.
        /// </summary>
        /// <param name="telemetryName">
        /// String key of the Telemetry to be stored.
        /// </param>
        /// <param name="telemetryObject">
        /// ITelemetry object to be stored with the telemetryName string.
        /// </param>
        /// <returns>Returns status of the register operation.</returns>
        public bool RegisterTelemetry(string telemetryName, ITelemetry telemetryObject)
        {             
            try
            {
                Instance.RegisteredTelemetry.Add(telemetryName, telemetryObject);
                return true;
            }
            catch (ArgumentException)
            {
                MastiDiagnostics.LogWarning(string.Format(
                    CultureInfo.CurrentCulture,
                    "Failed to register telemetry for {0}. Key already exists.",
                    telemetryName));
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(
                    CultureInfo.CurrentCulture,
                    "Failed to register telemetry for {0}. {1}",
                    telemetryName,
                    e.Message));
            }

            return false;
        }

        /// <summary>
        /// Write the data within each of the registeredTelemetry objects to a file.
        /// </summary>
        /// <returns>Return the status of the store operation.</returns>
        public bool StoreTelemetry()
        {
            string content;
            try
            {
                // Serialize the accumulated telemetry data.
                content = JsonConvert.SerializeObject(Instance.RegisteredTelemetry);
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to write telemetry. {0}", e.Message));
                return false;
                throw;
            }

            // Path to base directory.
            string directory = System.AppContext.BaseDirectory;

            // Get current date.
            string date = DateTime.Now.ToString("MM_dd_yyyy", CultureInfo.CurrentCulture);

            // Select files in the base directory belonging to this date.
            string[] files = Directory.GetFiles(directory, "Telemetry_" + date + '*');

            var telemetryNumber = 0;

            if (files != null && files.Length != 0)
            {
                // Get the current log number.
                string[] tokens = files.Max().Split('_');
                string token = tokens[tokens.Length - 1];
                telemetryNumber = int.Parse(token.Split('.')[0], CultureInfo.CurrentCulture) + 1;
            }

            // Generate file name with obtained details.
            var fileName = string.Format(CultureInfo.CurrentCulture, "Telemetry_{0}_{1}.json", date, telemetryNumber);
            var filePath = directory + fileName;

            try
            {
                // Initialize stream writer for the telemetry file.
                using (System.IO.StreamWriter telemetryWriter = new System.IO.StreamWriter(filePath, false))
                {
                    telemetryWriter.Write(content);
                }
            }
            catch (Exception e)
            {
                MastiDiagnostics.LogError(string.Format(CultureInfo.CurrentCulture, "Failed to write telemetry. {0}", e.Message));
                return false;
            }

            return true;
        }
    }
}
