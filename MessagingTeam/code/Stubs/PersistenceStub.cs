﻿//-----------------------------------------------------------------------
// <author> 
//    Gautam Kumar
// </author>
//
// <date> 
//     11-oct-2018 
// </date>
// 
// <reviewer> 
//     Aryan Raj
// </reviewer>
// 
// <copyright file="PersistenceStub.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      The following file contain our SchemaStub.
// </summary>
//-----------------------------------------------------------------------

namespace Masti.Messenger.Stubs
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Masti.Persistence;

    /// <summary>
    /// dummy object instance of persistence
    /// </summary>
    public class PersistenceStub : IPersistence
    {
        /// <summary>
        /// dummy function for deleting message
        /// </summary>
        /// <param name="startSessionId">Start Session</param>
        /// <param name="endSessionId">End Session</param>
        /// <returns>Return the success or failure of deleting the messages</returns>
        public int DeleteSession(int startSessionId, int endSessionId)
        {
            return 1;
        }

        /// <summary>
        /// gives current session
        /// </summary>
        /// <returns>1 or 0</returns>
        public int GetCurrentSessionId()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// dummy function for reterving message
        /// </summary>
        /// <param name="startSessionId">Start Session</param>
        /// <param name="endSessionId">End Session</param>
        /// <returns>List of string</returns>
        Collection<string> IPersistence.RetrieveSession(int startSessionId, int endSessionId)
        {
            Collection<string> collection = new Collection<string>
            {
                "Success"
            };
            return collection;
        }

        /// <summary>
        /// a dummy function to save session
        /// </summary>
        /// <param name="message">message to store</param>
        /// <returns>Return the success or failure of storing the messages</returns>
        public bool SaveSession(string message)
        {
            return true;
        }
    }
}
