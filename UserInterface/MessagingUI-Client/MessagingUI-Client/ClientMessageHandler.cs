﻿//-----------------------------------------------------------------------
// <author> 
//     Polu Varshith
// </author>
//
// <date> 
//     17-Nov-2018 
// </date>
// 
// <reviewer> 
//     
// </reviewer>
// 

// <copyright file="MessagingUIClient" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
//-----------------------------------------------------------------------

namespace Masti.MessagingUIClient
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;
    using Masti.ImageProcessing;
    using Messenger;

    /// <summary>
    /// Defines the <see cref="ClientChatScreen" />
    /// </summary>
    public partial class ClientChatScreen : Form
    {
        /// <summary>
        /// Receiver message handler
        /// </summary>
        /// <param name="message">The message<see cref="string"/></param>
        /// <param name="serverIP">The to IP<see cref="string"/></param>
        /// <param name="clientIP">The from IP<see cref="string"/></param>
        /// <param name="dateTime">The dateTime<see cref="string"/></param>
        public void ReceiverMessageHandler(string message, string serverIP, string clientIP, string dateTime)
        {
            this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), "Server", message, "true");
        }

        /// <summary>
        /// Status handler for sent messages
        /// </summary>
        /// <param name="status">The status<see cref="Messenger.Handler.StatusCode"/></param>
        /// <param name="fromIP">The from IP<see cref="string"/></param>
        /// <param name="message">The message<see cref="string"/></param>
        public void SendingStatusHandlers(Messenger.Handler.StatusCode status, string fromIP, string message)
        {
            // if connection fails
            if (string.Equals(status.ToString(), "ConnectionError", StringComparison.Ordinal))
            {
                MessageBox.Show("Connection Failed");
            }
            //connection is established
            else if (string.Equals(status.ToString(), "ConnectionEstablished", StringComparison.Ordinal))
            {
                this.BeginInvoke(new ChangeConnectButtonDelegate(this.ChangeConnectButton));
            }
            // Message is successfully sent
            else if (string.Equals(status.ToString(), "Success", StringComparison.Ordinal))
            {
                this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), "Client", message, "true");
            }
            // Sending Message failed
            else if (string.Equals(status.ToString(), "Failure", StringComparison.Ordinal))
            {
                this.BeginInvoke(new InvokeMessageDelegate(this.InvokeMessage), "Client", message, "false");
            }
        }
    }
}
