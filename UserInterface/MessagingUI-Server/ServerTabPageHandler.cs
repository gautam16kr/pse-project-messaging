﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Reorder tab pages on demand and some functionality on tab page change.
// </summary>
// <copyright file="ServerTabPageHandler.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// Tab page changed.
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="EventArgs"/></param>
        private void SelectedIndexChanged(object sender, EventArgs e)
        {
            TabPage tabPage = ServerChatSectionTabs.SelectedTab;
            string clientIP = GetClientName(tabPage.Text);
            this.PanelSizeChanged(null, null);
            TextBox unreadMessageCountTextBox = GetControl(tabPage.Controls, clientIP + "unreadMessage") as TextBox;
            string unreadMessageCount = "00"; // Reset unread message count.
            unreadMessageCountTextBox.Text = unreadMessageCount;

            SplitContainer splitContainer = GetControl(tabPage.Controls, clientIP + "split") as SplitContainer;
            //// Set the screen share button image based on the current status.
            if (splitContainer.Panel1Collapsed)
            {
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\startScreen.png");
            }
            //// Starts the screen sharing for the client if it was earlier on.
            else
            {
                // Call start screen share function here.
                shareScreenButton.Image = Image.FromFile("..\\..\\Images\\stopScreen.png");
                this.previousScreenSharingClient = tabPage;
            }

            //// Stops the screen sharing for the previous client on tab change.
            if (this.previousScreenSharingClient != null)
            {
                string previousClientIP = GetClientName(this.previousScreenSharingClient.Name);

                // Call stop screen share function here.
            }
        }

        /// <summary>
        /// Reorder tab pages.
        /// </summary>
        /// <param name="tabPage">Tab page<see cref="TabPage"/></param>
        private void ReorderTabs(TabPage tabPage)
        {
            TabPage currentPage = ServerChatSectionTabs.SelectedTab;
            try
            {
                ServerChatSectionTabs.TabPages.Remove(tabPage);
            }
            catch (Exception e)
            {
            }

            ServerChatSectionTabs.TabPages.Insert(0, tabPage);
            ServerChatSectionTabs.SelectTab(ServerChatSectionTabs.TabPages.IndexOf(currentPage));
        }
    }
}
