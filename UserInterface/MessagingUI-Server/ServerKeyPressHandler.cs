﻿//-----------------------------------------------------------------------
// <author>
//      Jayaprakash A
// </author>
// <reviewer>
//      K Durga Prasad Reddy
// </reviewer>
// <date>
//      17-Nov-2018
// </date>
// <summary>
//      Process keystrokes to handle page scroll and send message
// </summary>
// <copyright file="ServerKeyPressHandler.cs" company="B'15, IIT Palakkad">
//     This project is licensed under GNU General Public License v3. (https://fsf.org) 
// </copyright>
//-----------------------------------------------------------------------

namespace Masti.MessagingUIServer
{
    using System;
    using System.Drawing;
    using System.Windows.Forms;

    /// <summary>
    /// Defines the <see cref="ServerChatScreen" />
    /// </summary>
    public partial class ServerChatScreen : Form
    {
        /// <summary>
        /// The scroll function.
        /// </summary>
        /// <param name="delta">The delta<see cref="int"/></param>
        /// <param name="panel">The panel<see cref="ScrollableControl"/></param>
        public static void Scroll(int delta, ScrollableControl panel)
        {
            if (panel == null)
            {
                return;
            }
            int newYPosition = Math.Abs(panel.AutoScrollPosition.Y) + delta; // Calculates the new scroll position.
            //// Changes the scroll position.
            if (newYPosition > 0)
            {
                panel.AutoScrollPosition = new Point(
                       panel.AutoScrollPosition.X, newYPosition);
            }
            else
            {
                panel.AutoScrollPosition = new Point(
                       panel.AutoScrollPosition.X, 0);
            }
        }

        /// <summary>
        /// The process command key.
        /// </summary>
        /// <param name="msg">The message<see cref="Message"/></param>
        /// <param name="keyData">The key data<see cref="Keys"/></param>
        /// <returns>Bool<see cref="bool"/></returns>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            string clientName = null;
            try
            {
                clientName = ServerChatSectionTabs.SelectedTab.Name;
            }
            catch (Exception)
            {
                return base.ProcessCmdKey(ref msg, keyData);
            }

            string clientIP = GetClientName(clientName);

            TabPage messagePage = this.GetTabPage(clientIP + "tabPage");
            SplitContainer splitContainer = GetControl(messagePage.Controls, clientIP + "split") as SplitContainer;
            SplitterPanel panel = splitContainer.Panel2;

            if (panel.Bounds.Contains(this.PointToClient(Cursor.Position)))
            {
                switch (keyData)
                {
                    case Keys.PageDown: // Scroll down event.
                        {
                            Scroll(10, panel);
                            return true;
                        }

                    case Keys.PageUp: // Scroll up event.
                        {
                            Scroll(-10, panel);
                            return true;
                        }
                }
            }

            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// Handles ctrl + enter to send message. 
        /// </summary>
        /// <param name="sender">The sender<see cref="object"/></param>
        /// <param name="e">The e<see cref="KeyEventArgs"/></param>
        private void ServerChatScreenKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.Enter)
            {
                if (serverMessageTextBox.Focused)
                {
                    this.SendMessage(null, null);
                    serverMessageTextBox.Text = string.Empty; // Reset the reply message.
                }
            }
        }
    }
}
