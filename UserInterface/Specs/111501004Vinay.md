# Indian Institute Of Technology, Palakkad

###                                  Principles of Software Engineering Lab Project

##### 									                                                                           UI TEAM



​																                                      Vinay Krishna A
​                                                                                                                                                                       111501004			



**Objective**

- Screen sharing UI element Design.
- Code the functions based on events using Image processing API for screen sharing on both client and server side.



**Design**

- **Screen Sharing** :

	The screen sharing on the Server side application is started after the screen sharing button is pressed on client side application.

  ​	New window was choosen for screen sharing at first.But as it complicates the program it is dropped and choosen to replace the messaging dialogue box and send message box after screen sharing on the Server side.And a stop button on the bottom to stop the screen sharing.

  ​	The below image is a sample UI design that shows the screen on client side application with a screen sharing button.

  ![](./111501004Vinay/ClientScreenSharingButton.png)

	The below image is a sample UI that shows the screen that shows the screen of the server side application before screen sharing.

  ![](./111501004Vinay/ServerSide.png)

	The below image is a sample UI that shows the screen on the server side application after screen sharing with a close button for stopping your screen sharing.

  ![](./111501004Vinay/ScreenShare.png)


##### Component wise description

1. **Screen Sharing Button**: After recieving a request from server side for screen sharing the client can press the screen sharing button to start screen sharing.
2. **Close Screen Sharing Button:** After the Screen sharing the server can stop the screen sharing with the client using the close button.


**Class Diagram**

​	The image shown below has all the classes involved and the interaction between them.

![](./111501004Vinay/ServerClassDiagram.png)
