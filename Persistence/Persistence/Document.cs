﻿//-----------------------------------------------------------------------
// <author> 
//     Prabal Vashisht
// </author>
//
// <date> 
//     25/10/2018
// </date>
// 
// <reviewer> 
//     Prabal Vashisht 
// </reviewer>
// 
// <copyright file="Document.cs" company="B'15, IIT Palakkad">
//    This project is licensed under GNU General Public License v3. (https://fsf.org)
// </copyright>
// 
// <summary>
//      This module is used to save, retrieve and delete messages from the database.
// </summary>
//-----------------------------------------------------------------------
namespace Masti.Persistence
{
    using MongoDB.Bson;

    /// <summary>
    /// "Document" class represents the structure of each document inserted in a collection.
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Gets or sets the id of document stored in the database.
        /// </summary>
        public ObjectId _id { get; set; }

        /// <summary>
        /// Gets or sets the session ID of each message stored.
        /// </summary>
        public int SessionId { get; set; }

        /// <summary>
        /// Gets or sets the message stored inside the database.
        /// </summary>
        public string Message { get; set; }
    }
}
